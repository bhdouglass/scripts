Random Scripts for Linux
========================

**volman**: A script for managing your volume.  Useful for Window Managers like Openbox.  Just attach *volman up*, *volman down*, and *volman toggle* to your volume up, down, and mute keys respectively.  Volman changes your volume and user *notify-send* to popup a notification.  View the script for all options and configuration.

**wallpaper**: Constantly changes wallpaper to a random one using *feh* from a specified directory.

**displays**: Connect/disconnect a display using *xrandr*